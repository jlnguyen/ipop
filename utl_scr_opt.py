# ==============================================================
'''
SCRIPT:
    utl_scr_opt.py

PURPOSE:
    - SCR optimisation functions
    
INPUTS:

OUTPUTS:


DEVELOPMENT:
    Version 1.0 - Joseph Nguyen | 23Apr2020
    ---
'''
# ==============================================================
# -------------------------------------------------

import os
import sys
import importlib
import pandas as pd
import numpy as np
import numbers
from scipy import optimize
import matplotlib.pyplot as plt
import seaborn as sns
import plotly.graph_objects as go


def get_diff_from_first(df, col='scr', col_grp='crn', cols_new=['diff', 'diff_perc']):
    '''Difference in values between first rank and all subsequent rows
    '''
    cols_new = [col + '_' + c for c in cols_new]
    col_diff = cols_new[0]
    col_diff_perc = cols_new[1]
    df[col_diff] = np.nan
    df[col_diff] = np.nan

    # Append first rank row value as new column
    col_first = col + '_first'
    df = df.merge(
        df.groupby(col_grp)[[col]].first().reset_index().rename(columns={col: col_first}), on=col_grp
    )

    # Difference between first and subsequent rows
    df[col_diff] = df[col] - df[col_first]
    df[col_diff_perc] = df[col_diff] / df[col_first] * 100

    # Remove {col_first}
    df.drop(columns=col_first, inplace=True)

    return df


def crt_scr_gradient(df):
    '''Create scr gradient column
    '''
    # Differences and gradient
    df = get_diff_from_first(df, 'inc_sales')
    df = get_diff_from_first(df, 'target_cost')
    df['scr_gradient'] = df['inc_sales_diff'] / df['target_cost_diff']

    return df


def choose_offer(df, scr_gradient_ub):
    '''Select best or 2nd best offer based on SCR gradient

    Args:
        df (df): crn-offer containing top two offers per crn, defined by {rank}
        scr_gradient_ub (float or list/array): SCR gradient upper bound

    Returns:
        df (df): crn-offer containing selected single offer per crn, with column for each "scr_gradient_ub" set
    '''
    if isinstance(scr_gradient_ub, numbers.Number):
        scr_gradient_ub = [scr_gradient_ub]
    if isinstance(scr_gradient_ub, np.ndarray) and scr_gradient_ub.shape == ():
        scr_gradient_ub = [scr_gradient_ub.tolist()]

    df_ls = []
    for i in range(len(scr_gradient_ub)):

        # Choose 2nd best
        cond = (
            (df['rank'] == df['rank_2nd']) &
            (df['target_cost_diff'] < 0) &
            (df['scr_gradient'] <= scr_gradient_ub[i])
        )
        df_2nd = df[cond]

        # Choose remaining best
        cond = ~df['crn'].isin(df_2nd['crn']) & (df['rank'] == df['rank_best'])
        df_1st = df[cond]

        df_out = pd.concat((df_1st, df_2nd), axis=0)
        df_out['scr_gradient_ub'] = scr_gradient_ub[i]

        df_ls.append(df_out)

    return pd.concat(df_ls)


def crt_label_col(
        _df, catg_col,
        is_rank=True, campaign_level=True, cvm=True, multiplier=True, is_offer=False,
        campaign_type_incl=[], campaign_level_incl=[], cvm_multiplier_incl=[],
        rank_incl=[],
        suffix=None):
    '''Create custom label column appending str columns in df

    - There are 4 main levels; all can be toggled on or off:
        1. is_offer = {offer, no-offer}
        2. campaign_type = {tu, ss, mp, na} *DEFAULT COLUMN* (cannot be toggled off)
        3. campaign_level = {store, category, None}
        4. cvm_multiplier = [cvm]: {MVML, etc...} OR [multiplier]: {2,3,5} 

    Args:
        _df (df)
        catg_col (str): new category column name
        is_rank (bool): toggle to include "offer rank"
        campaign_level (bool): toggle level
        cvm_multiplier (bool): toggle level
        is_offer (bool): toggle level

        campaign_type_incl (list): filter to include
        campaign_level_incl (list): filter to include
        cvm_multiplier_incl (list): filter to include
        rank_incl (list): filter to include (str values)
        suffix (str): letter suffix add to category column -
            use to determine src to tgt

    Return:
        df (df): df with new column "catg_col"
    '''
    df = _df.copy()
    df.drop(columns=catg_col, errors='ignore', inplace=True)

    df['is_offer'] = np.where(df['offerId'] != 'na', 'offer', 'no-offer')
    df['rank'] = df['rank'].astype('str')
    df['multiplier'] = df['multiplier'].values.astype('str')

    # Filters on cvm and multiplier
    if not cvm:
        df['cvm'] = 'cvm-off'
    if not multiplier:
        df['multiplier'] = 'mp-off'
    df['cvm_multiplier'] = np.where(
        df['campaign_type'] == 'mp', df['multiplier'], df['cvm'])
    cvm_multiplier = cvm or multiplier

    # Apply filters
    cond = [True] * df.shape[0]
    if len(campaign_type_incl) > 0:
        cond &= df['campaign_type'].isin(campaign_type_incl)
    if len(campaign_level_incl) > 0:
        cond &= df['campaign_level'].isin(campaign_level_incl)
    if len(cvm_multiplier_incl) > 0:
        cond &= df['cvm_multiplier'].isin(cvm_multiplier_incl)
    if len(rank_incl) > 0:
        cond &= df['rank'].isin(rank_incl)
    df = df[cond]

    # Category column
    df[catg_col] = df['campaign_type']
    if is_rank:
        df[catg_col] = df['rank'] + ' | ' + df[catg_col]
    if campaign_level:
        df[catg_col] = df[catg_col] + ' | ' + df['campaign_level']
    if cvm_multiplier:
        df[catg_col] = df[catg_col] + ' | ' + df['cvm_multiplier']
    if is_offer:
        df[catg_col] = df[catg_col] + ' | ' + df['is_offer']
    if suffix is not None:
        df[catg_col] = df[catg_col] + ' | ' + suffix

    # output df
    cols = [c for c in _df.columns.tolist() if c != catg_col]
    cols = [catg_col] + cols

    return df[cols]


def crt_smry(df, catg_col):
    '''Group by ["catg_col", "rank"] and return summary for crn count, inc_sales, target_cost
    '''
    df_smry = df.groupby([catg_col, 'rank']).agg({
        'crn': ['count'],
        'inc_sales': ['sum', 'mean'],
        'target_cost': ['sum', 'mean'],
    })
    cols = [mi[0] + '_' + mi[1] for mi in df_smry.columns.tolist()]
    df_smry.columns = cols
    df_smry.reset_index(inplace=True)

    return df_smry


def crt_sankey_data(src, tgt, catg_col):
    '''Merge source and target dfs to create sankey data

    Args:
        src (df): source of crn-offers (best offer)
        tgt (df): target of crn-offers (optimised SGUB offer)
        catg_col (str): category column

    Returns:
        link (dict): {source, target, value} input for plotly sankey diagram: integer indexes
        label (list): dictionary keys (str labels) of the link integer indexes
        sankey (df): {source, target, value} in df format
    '''
    trans = src.merge(tgt, on='crn', how='outer', suffixes=('_src', '_tgt'))
    sankey = trans.groupby([f'{catg_col}_src', f'{catg_col}_tgt'])['crn'].count().\
        to_frame('value').\
        rename_axis(['source', 'target']).\
        reset_index()

    # Map src and tgt elements to numbers
    k = set(sankey['source'].tolist() + sankey['target'].tolist())
    v = np.arange(0, len(k))
    sankey_dict = dict(zip(k, v))

    # Map
    sankey['source'] = sankey['source'].map(sankey_dict)
    sankey['target'] = sankey['target'].map(sankey_dict)

    # transition link
    link = dict(
        source=sankey['source'].tolist(),
        target=sankey['target'].tolist(),
        value=sankey['value'].tolist(),
    )

    # labels
    label = list(sankey_dict.keys())

    return link, label, sankey


# ==============================================================
# Optimisation
# ==============================================================
def obj_func(x, df, base_metrics, scenario='opt') -> float:
    '''Return profit gain from cost savings less inc-sales loss. Return negative "profit_gain" to minimise.

    Args:
        x (float): SCR gradient UB (SGUB)
        df: scored crn-offer table containing top two offers
        base_metrics (series): (inc-sales, cost, profit) total of baseline: best offer only
        scenario (str): {opt, rpt} optimisation or reporting metrics:
            {inc_sales_loss, cost_saving, profit_gain, inc_sales_loss_perc, cost_saving_perc, profit_gain_perc}

    Returns:
        -profit_gain (float or dict): profit gain from cost savings less inc-sales loss (negative to minimise); dict for reporting
    '''
    # Select best crn-offer
    sel = choose_offer(df, x)

    # Get total inc-sales and cost
    total = sel.groupby('scr_gradient_ub')['inc_sales', 'target_cost'].sum()

    # inc-sales loss and cost savings from selection over baseline
    delta = total - base_metrics

    # Profit gain from cost savings less inc-sales loss
    profit_gain = -delta['target_cost'] + delta['inc_sales']

    switcher = {
        'opt': -profit_gain.values,
        'rpt': {
            'inc_sales_loss':      delta['inc_sales'].values,
            'cost_saving': -delta['target_cost'].values,
            'profit_gain':         profit_gain.values,
            'inc_sales_loss_perc': delta['inc_sales'].values / base_metrics['inc_sales'] * 100,
            'cost_saving_perc': -delta['target_cost'].values / base_metrics['target_cost'] * 100,
            'profit_gain_perc':    profit_gain.values / base_metrics['profit'] * 100,
            # Totals
            'inc_sales_total': total['inc_sales'].values,
            'cost_total':      total['target_cost'].values,
            'profit_total':    total['inc_sales'].values - total['target_cost'].values,
            'count':           sel.shape[0]
        }
    }
    return switcher.get(scenario, -profit_gain.values)


def constr_ineq(x, df, base_constr_metrics, lb=[-5], ub=[5, 5]):
    '''Inequality constraints; upper bounds

    Args:
        x (float): SCR gradient UB (SGUB)
        df: scored crn-offer table containing top two offers
        base_constr_metrics (series): (inc-sales, p_rdm, p_open, p_unsub) total of baseline best offer
        lb (list): lower bound on perc loss for [p_unsub]
        ub (list): upper bound on perc loss for [p_rdm, p_open]

    Returns:
        con (1D array): ineq-constraints;
            -> to unpivot into constraints use: con.reshape(3,-1)

    '''
    # Select best crn-offer
    sel = choose_offer(df, x)

    # Get totals
    cols = base_constr_metrics.index
    total = sel.groupby('scr_gradient_ub')[cols].sum().reset_index(drop=True)

    # Constraints perc loss
    con_rdm = -(total.iloc[:, 0] - base_constr_metrics[0]
                ) / base_constr_metrics[0] * 100
    con_open = -(total.iloc[:, 1] - base_constr_metrics[1]
                 ) / base_constr_metrics[1] * 100
    con_unsub = -(total.iloc[:, 2] - base_constr_metrics[2]
                  ) / base_constr_metrics[2] * 100

    # Constraints ub: [p_rdm, p_open]; constr_val ≤ ub <=> ub - constr_val ≥ 0
    ub_c1 = ub[0] - con_rdm.values
    ub_c2 = ub[1] - con_open.values

    # Constraints lb: [p_unsub]; lb ≤ constr_val <=> constr_val - lb ≥ 0
    lb_c1 = con_unsub.values - lb[0]

    return np.concatenate((ub_c1, ub_c2, lb_c1))


def ipop(_base, _best, score='inc_sales',
         n_loop=20, i_max=None, change_lim=3,
         x0=0.5, lb=[-1], ub=[3, 3],
         verbose=False, out='all'):
    '''
    Iterative Profit Optimise (IPOP):

        Constrained optimisation to maximise profit gain.

        - For each crn, rank offers in descending order of inc-sales (highest first)

        - For the top two offers, calculate the sales-cost gradient (SCG)
           - Limit to cases where the 2nd best offer reduces cost
        - Find the optimal sales-cost gradient upper bound (SCGUB)
            - constrain the perc loss in [p_rdm, p_open]
            - constrain the perc increase in [p_unsub]
        - Select the 2nd best offer if SCG ≤ SCGUB; otherwise, select the best offer
        - Reassign the 2nd best offer as the best offer and identify the next 2nd best offer

        - Repeat until offers do not switch

    Args:
        _base (df): crn-offers
        _best (df): initial best crn-offer
        score (str): metric to rank on
        n_loop (int): num.iterations
        i_max (int): maximum iteration (early stopping)
        change_lim (int): limit on number of non-improving offers evaluated
        x0 (float): gradient UB initial value
        lb (list): lower bound on perc loss: [p_unsub]
        ub (list): upper bound on perc loss: [p_rdm, p_open]
        verbose (bool): print or not
        out (str): {'all', 'min'} return "best" with all (or minimal) columns

    Returns:
        res (object):
        res.best_crn_offer (df): optimised best crn-offer
        res.base_crn_offer (df): updated base crn-offers
        res.iter (int): final iteration
        res.x_opt (float): final iteration x_opt
        res.opt (dict): report metrics for final iteration
    '''
    pd.options.mode.chained_assignment = None

    # Intialise
    opt_cols = [
        'crn',  # 'offerId',
        'inc_sales', 'target_cost', 'score',
        'p_rdm', 'p_open', 'p_unsub',
    ]
    sort_cols = ['crn', score, 'target_cost', 'score']
    sort_dir = [True, False, True, False]
    metric_cols = ['inc_sales', 'target_cost']
    constr_cols = ['p_rdm', 'p_open', 'p_unsub']

    # Limit columns for efficiency
    base = _base[opt_cols]
    best = _best[opt_cols]

    # Initial best offer metrics and constraints
    best0_metrics = best[metric_cols].sum()
    best0_metrics['profit'] = best0_metrics[0] - best0_metrics[1]
    best0_constr_metrics = best[constr_cols].sum()

    # Sort base by "inc_sales"; tie break on "target_cost", "score"
    base.sort_values(by=sort_cols, ascending=sort_dir, inplace=True)

    # Rank and flag current best and 2nd best offers
    base['rank'] = base.groupby('crn')[sort_cols[1]].rank(
        ascending=sort_dir[1], method='first').astype('uint8')
    base['rank_best'] = 1
    base['rank_2nd'] = 2
    base['no_change'] = 0

    # offer count per crn
    offer_cnt = base.groupby('crn')['rank'].max().values
    offer_cnt_explode = [[x]*x for x in offer_cnt]
    base['offer_cnt'] = [x for ls in offer_cnt_explode for x in ls]

    if i_max is None:
        i_max = n_loop

    # -------------------------------------------------
    # Iterate
    # -------------------------------------------------
    for i in range(n_loop):

        # Top 2 offers
        cond_top2 = (base['rank'] == base['rank_best']) | (
            base['rank'] == base['rank_2nd'])
        top2 = base[cond_top2]

        # Top 2 diff for SC gradient
        top2_diff = top2[metric_cols] - \
            top2.groupby('crn')[metric_cols].shift()
        top2 = top2.merge(top2_diff, left_index=True,
                          right_index=True, suffixes=('', '_diff'))
        top2['scr_gradient'] = top2['inc_sales_diff'] / \
            top2['target_cost_diff']

        # Optimise gradient UB
        x_opt = optimize.fmin_cobyla(
            obj_func, x0, constr_ineq,
            args=(top2, best0_metrics),
            consargs=(top2, best0_constr_metrics, lb, ub)
        )
        # Select best offer
        best = choose_offer(top2, x_opt)

        # Constraints (to check for violation where constraint(s) values impossible for all x)
        con = constr_ineq(x_opt, top2, best0_constr_metrics, lb, ub)

        if verbose:
            print(f'iter: {i+1}, x_opt: {x_opt.round(4)}')

        # -------------------------------------------------
        # Update base table where best has changed
        # -------------------------------------------------
        # Check if offers still exist
        cond_offers_exist = (base['rank_2nd'] < base['offer_cnt'])

        # Check if num.offer.changes has reached limit
        crn_changes_open = base.groupby('crn')['no_change'].sum() < change_lim
        cond_changes_open = base['crn'].isin(
            crn_changes_open[crn_changes_open].index) & cond_offers_exist

        # Offer change
        cond = (best['rank'] == best['rank_2nd'])
        crn_upd = best.loc[cond, 'crn']
        cond_change = base['crn'].isin(crn_upd)

        # Update flags for crns with offer change (2nd becomes best)
        cond = cond_changes_open & cond_change
        base['rank_best'] = np.where(cond, base['rank_2nd'], base['rank_best'])
        base['rank_2nd'] = np.where(cond, base['rank_2nd']+1, base['rank_2nd'])

        # Update flags for crns with no offer change
        cond0 = cond_changes_open & ~cond_change
        cond = cond0 & (base['rank'] == base['rank_2nd'])
        base['no_change'] = np.where(cond, 1, base['no_change'])
        base['rank_2nd'] = np.where(
            cond0, base['rank_2nd']+1, base['rank_2nd'])

        # -------------------------------------------------
        # Terminate:
        # offers exhausted OR
        # constraints violated (impossible for all x) OR
        # iterations reached user limit
        # -------------------------------------------------
        if (cond_changes_open.sum() == 0) | np.any(con < 0) | (i+1 == i_max):
            break

    # Results
    class Res(object):
        pass
    res = Res()
    res.best = _base.loc[best.index, :] if out == 'all' else best
    res.base = base
    res.iter = i
    res.x_opt = x_opt
    res.rpt = obj_func(x_opt, top2, best0_metrics, 'rpt')

    if verbose:
        if np.any(con < 0):
            print('>> IPOP terminated: constraint(s) violated')
        elif (cond_changes_open.sum() == 0):
            print('>> IPOP successful')
        elif i+1 == n_loop:
            print('>> IPOP reached iteration limit')
        elif i+1 == i_max:
            print('>> IPOP reached user iteration limit')
        else:
            print('>> IPOP Error')

    return res


# ==============================================================
# Reporting
# ==============================================================
def get_metrics_avg(_df, cols, col_count):
    '''Metrics divided by count
    '''
    df = _df.copy()

    cols_avg = [c + '_avg' for c in cols]
    for c, ca in zip(cols, cols_avg):
        df[ca] = df[c] / df[col_count]

    return df


def get_metrics_smry(df, cols=['inc_sales', 'target_cost', 'profit'], out='all'):
    '''Metrics total and percentage

    Args:
        out (str): {'all', 'tot', 'avg'}

    '''
    # Include inc-sales and cost for scr calc
    base_cols = ['profit', 'inc_sales', 'target_cost']
    for c in cols:
        if c not in base_cols:
            base_cols.append(c)

    df['profit'] = df['inc_sales'] - df['target_cost']

    df_smry = df[base_cols].sum()
    df_smry['count'] = df.shape[0]
    df_smry['scr'] = df_smry['inc_sales'] / df_smry['target_cost']
    df_smry = get_metrics_avg(df_smry, base_cols, 'count')

    if out == 'all':
        return df_smry
    elif out == 'avg':
        cols_avg = [c for c in df_smry.index if c[-4:] == '_avg']
        return df_smry[cols_avg]
    elif out == 'tot':
        cols_tot = [c for c in df_smry.index if c[-4:] != '_avg']
        return df_smry[cols_tot]


def get_metrics_by_camp_type(df):

    map_reward_cat = {'L': 1, 'M': 2, 'H': 3}
    df['reward_cat_idx'] = df['reward_cat'].map(map_reward_cat)

    df_camp_type = df.groupby(['campaign_type']).agg({
        'crn': 'count',
        'reward_cat_idx': 'mean',
        'multiplier': 'mean',
        'inc_sales': 'mean',
        'target_cost': 'mean',
        'profit': 'mean', }).reset_index()
    df_camp_type['scr'] = df_camp_type['inc_sales'] / \
        df_camp_type['target_cost']

    df_camp_type['reward/multiplier'] = np.where(
        df_camp_type['campaign_type'] == 'ss', df_camp_type['reward_cat_idx'], df_camp_type['multiplier']
    )
    df_camp_type.drop(columns=['reward_cat_idx', 'multiplier'], inplace=True)

    return df_camp_type


# ==============================================================
# PLOTS
# ==============================================================
def plot_dual_metrics(df, x='scr_gradient_ub', y1='score', y2='target_cost',
                      title=None, line_x0=True,
                      fig=None, ax=None,
                      x_opt=None, y_opt=None, text_opt=None, text_y_pos=None,):
    '''Plot metric y1 vs x, and y2 vs x (dual axis)
    Args:
        y_opt (series): column-value to print
    '''
    sns.set_palette(sns.color_palette("muted", 10))

    if fig is None:
        fig, ax = plt.subplots()
#     sns.lineplot(data=df, x=x, y=y1, ax=ax, label=y1.capitalize())
    sns.lineplot(data=df, x=x, y=y1, ax=ax,
                 color='tab:blue', label=y1.capitalize())
    if line_x0:
        ax.axvline(x=0, c='k', ls='--')

    ax2 = ax.twinx()
#     sns.lineplot(data=df, x=x, y=y2, ax=ax2, label=y2.capitalize())
    sns.lineplot(data=df, x=x, y=y2, ax=ax2,
                 color='tab:orange', label=y2.capitalize())

    # legend
    ax.legend(loc='upper center', bbox_to_anchor=(1.3, 1))
    ax2.legend(loc='upper center', bbox_to_anchor=(1.3, 0.89))

    if title is None:
        ax.set_title(
            f'{y1.capitalize()} and {y2.capitalize()} vs {x.capitalize()}')
    else:
        ax.set_title(title)

    # x optimal
    if x_opt is not None:
        ax.axvline(x=x_opt, c='limegreen', ls='--', lw=3)

        if text_opt is None:
            text_opt = f'{x}*: {np.round(x_opt,2)}'
            if y_opt is not None:
                for j in range(len(y_opt)):
                    text_opt += f'\n{y_opt.index[j]}: {y_opt[j].round(2)}'

        x_lims = ax2.get_xlim()
        x_range = x_lims[1] - x_lims[0]
        x_delta = x_range * 0.05
        x_text_pos = x_opt + x_delta

        y_lims = ax2.get_ylim()
        y_range = y_lims[1] - y_lims[0]
        y_delta = y_range * 0.05
        if text_y_pos is None:
            text_y_pos = (y_lims[0] + 0.8 * y_range) + y_delta

        ax2.text(x=x_text_pos, y=text_y_pos, s=text_opt,
                 fontsize=14, verticalalignment='top',
                 bbox=dict(facecolor='yellow', alpha=0.3))

    return fig, ax, ax2


def plot_multi_metrics(df, x='scr_gradient_ub',
                       metric_ls=['inc_sales',
                                  'target_cost', 'inc_sales_cost'],
                       title=None, xlabel=None, ylabel=None,
                       fig=None, ax=None,
                       x_opt=None, y_opt=None, text_opt=None, text_y_pos=None,):
    '''Plot multiple metrics using a single axis; ensure metrics are on same scale
    '''
    sns.set_palette(sns.color_palette("muted", 10))

    # text names
    t_x = x.replace('_', ' ').capitalize()
    t_y = metric_ls[0].replace('_', ' ').capitalize()
    t_metric_ls = [m.replace('_', ' ').capitalize() for m in metric_ls]

    if fig is None:
        fig, ax = plt.subplots()

    for i in range(len(metric_ls)):
        m = metric_ls[i]
        t_m = t_metric_ls[i]
        ax.plot(df[x], df[m], '-', label=t_m)
    ax.axvline(x=0, c='k', ls='--')

    ax.set_title(title)
    ax.set_xlabel(t_x) if xlabel is None else ax.set_xlabel(xlabel)
    ax.set_ylabel(t_y) if ylabel is None else ax.set_ylabel(ylabel)
    ax.legend(loc='upper center', bbox_to_anchor=(1.2, 1))
    ax.autoscale(enable=True, axis='x', tight=True)

    # x optimal
    if x_opt is not None:
        ax.axvline(x=x_opt, c='limegreen', ls='--', lw=3)

        if text_opt is None:
            text_opt = f'{t_x}*: {np.round(x_opt,2)}'
            if y_opt is not None:
                for j in range(len(y_opt)):
                    text_opt += f'\n{t_metric_ls[j]}: {np.round(y_opt[j],2)}'

        x_lims = ax.get_xlim()
        x_range = x_lims[1] - x_lims[0]
        x_delta = x_range * 0.05
        x_text_pos = x_opt + x_delta

        y_lims = ax.get_ylim()
        y_range = y_lims[1] - y_lims[0]
        y_delta = y_range * 0.05
        if text_y_pos is None:
            text_y_pos = (y_lims[0] + 0.8 * y_range) + y_delta

        ax.text(x=x_text_pos, y=text_y_pos, s=text_opt,
                fontsize=14, verticalalignment='top',
                bbox=dict(facecolor='yellow', alpha=0.3))

    return fig, ax


def plot_metrics_tile(df, is_diff=False, **kwargs):
    '''Metrics: score, inc-sales, cost, SCR

    Args:
        is_diff (bool): toggle to plot original metrics or metric diffs
    '''
#     global left, right, bottom, top, wspace, hspace
    sns.set_palette(sns.color_palette("muted", 10))

    y_score = 'score'
    y_inc_sales = 'inc_sales'
    y_cost = 'target_cost'
    y_scr = 'scr'

    if is_diff:
        y_score += '_diff'
        y_inc_sales += '_diff'
        y_cost += '_diff'
        y_scr += '_diff'

    fig, ax = plt.subplots(2, 2, figsize=(15, 9))
    fig.subplots_adjust(hspace=0.35)
#     fig.subplots_adjust(left, bottom, right, top, wspace, hspace)

    sns.lineplot(data=df, x='scr_gradient_ub', y=y_score, ax=ax[0, 0])
    ax[0, 0].axvline(x=0, c='k', ls='--')
    ax[0, 0].set_title(y_score.capitalize())

    sns.lineplot(data=df, x='scr_gradient_ub', y=y_inc_sales, ax=ax[0, 1])
    ax[0, 1].axvline(x=0, c='k', ls='--')
    ax[0, 1].set_title(y_inc_sales.capitalize())

    sns.lineplot(data=df, x='scr_gradient_ub', y=y_cost, ax=ax[1, 0])
    ax[1, 0].axvline(x=0, c='k', ls='--')
    ax[1, 0].set_title(y_cost.capitalize())

    sns.lineplot(data=df, x='scr_gradient_ub', y=y_scr, ax=ax[1, 1])
    ax[1, 1].axvline(x=0, c='k', ls='--')
    ax[1, 1].set_title(y_scr.capitalize())

    return fig


def plot_sankey(link, label, pad=10, thickness=50, title='Sankey Diagram'):
    '''Plot sankey diagram

    Args:
        link (dict): {source, target, value} input for plotly sankey diagram: integer indexes
        label (list): dictionary keys (str labels) of the link integer indexes
        pad (int): size of vertical space between bars
        thickness (int): size of horizontal width of bars
        title_text (str): title name

    Returns:
        fig (hdl): figure handle
    '''
    fig = go.Figure(data=[go.Sankey(
        node=dict(
            pad=pad,
            thickness=thickness,
            line=dict(color="white", width=0.5),
            label=label,
            #         color = ["blue", "green", 'green', 'red']
        ),
        link=link
    )])

    fig.update_layout(title_text=title, font_size=12)
    # fig.show()

    return fig
