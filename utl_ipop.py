# ==============================================================
"""
SCRIPT:
    utl_ipop.py

PURPOSE:
    - IPOP dedicated functions
    - only include minimal functions required

INPUTS:

OUTPUTS:


DEVELOPMENT:
    Version 1.0 - Joseph Nguyen | 02May2020
    ---
    Version 1.1 - Joseph Nguyen | 10Jun2020
        - Align with DE BWS script
    ---
    Version 1.2 - Joseph Nguyen | 12Jun2020
        - Constraints UB: p_rdm loss, inc-sales loss
"""
# ==============================================================
# -------------------------------------------------

import pandas as pd
import numpy as np
import numbers
from scipy import optimize


class Config(object):
    pass


Config = Config()
Config.IPOP_N_LOOP = 25
Config.IPOP_CHANGE_LIM = 3
Config.IPOP_X0 = 0.5
Config.IPOP_LB = [-np.inf]
Config.IPOP_UB = [3, 3]


def _choose_offer(df, sc_grad_ub):
    """Select best or 2nd best offer based on sales-cost (SC) gradient

    Args:
        df (df): crn-offer containing top two offers per crn,
            defined by {rank}
        sc_grad_ub (float or list/array): SC gradient upper bound

    Returns:
        df (df): crn-offer containing best crn-offer;
            column "sc_grad_ub" for vector input
    """
    if isinstance(sc_grad_ub, numbers.Number):
        sc_grad_ub = [sc_grad_ub]
    elif isinstance(sc_grad_ub, np.ndarray) and sc_grad_ub.shape == ():
        sc_grad_ub = [sc_grad_ub.tolist()]

    df_ls = []
    for i in range(len(sc_grad_ub)):

        # Choose 2nd best
        cond = (
            (df["rank"] == df["rank_2nd"])
            & (df["target_cost_diff"] < 0)
            & (df["sc_grad"] <= sc_grad_ub[i])
        )
        df_2nd = df[cond]

        # Choose remaining best
        cond = ~df["crn"].isin(df_2nd["crn"]) & (
            df["rank"] == df["rank_best"])
        df_1st = df[cond]

        df_out = pd.concat((df_1st, df_2nd), axis=0)
        df_out["sc_grad_ub"] = sc_grad_ub[i]

        df_ls.append(df_out)

    return pd.concat(df_ls)


def _obj_func(x, df, base_metrics, scenario="opt"):
    """Return profit gain wrt "base_metrics". -ve profit gain to minimise

    Args:
        x (float): SC gradient UB (SCGUB)
        df: scored crn-offer table containing top two offers
        base_metrics (series): (inc-sales, cost, profit) total of baseline
            best offer only
        scenario (str): {opt, rpt} optimisation or reporting metrics

    Returns:
        -profit_gain (float or dict): profit gain (vectorised) or report
    """
    # Select best crn-offer
    sel = _choose_offer(df, x)

    total = sel.groupby("sc_grad_ub")["inc_sales", "target_cost"].sum()
    delta = total - base_metrics
    profit_gain = -delta["target_cost"] + delta["inc_sales"]

    if scenario == "opt":
        res = -profit_gain.values
    elif scenario == "rpt":
        res = {
            "inc_sales_loss": -delta["inc_sales"].values,
            "cost_saving": -delta["target_cost"].values,
            "profit_gain": profit_gain.values,
            # Perc
            "inc_sales_loss_perc": -delta["inc_sales"].values / \
            base_metrics["inc_sales"] * 100,
            "cost_saving_perc": -delta["target_cost"].values / \
            base_metrics["target_cost"] * 100,
            "profit_gain_perc": profit_gain.values / \
            base_metrics["profit"] * 100,
            # Totals
            "inc_sales_total": total["inc_sales"].values,
            "cost_total": total["target_cost"].values,
            "profit_total": total["inc_sales"].values - \
            total["target_cost"].values,
            "count": sel.shape[0],
        }
    return res


def _constr_ineq(x, df, base_constr_metrics,
                 lb=[-5], ub=[5, 5], scenario="opt"):
    """Inequality constraints

    Args:
        x (float): SC gradient UB (SCGUB)
        df: scored crn-offer table containing top two offers
        base_constr_metrics (series): (p_rdm, inc_sales)
            total of baseline best offer
        #lb (list): lower bound on perc loss for []
        ub (list): upper bound on perc loss for [p_rdm, inc_sales]
        scenario (str): {opt, rpt} optimisation or reporting metrics

    Returns:
        con (1D array): ineq-constraints;
            -> to unpivot into constraints use: con.reshape(3,-1)

    """
    # Select best crn-offer
    sel = _choose_offer(df, x)

    cols = base_constr_metrics.index
    total = sel.groupby("sc_grad_ub")[cols].sum().reset_index(drop=True)

    # Constraints perc loss
    con_rdm = (
        -(total.iloc[:, 0] - base_constr_metrics[0])
        / base_constr_metrics[0] * 100
    )
    con_inc_sales = (
        -(total.iloc[:, 1] - base_constr_metrics[1])
        / base_constr_metrics[1] * 100
    )
#     con_unsub = (
#         -(total.iloc[:, 2] - base_constr_metrics[2])
#         / base_constr_metrics[2] * 100
#     )

    # Constraints ub: [p_rdm, p_open];
    # constr_val ≤ ub <=> ub - constr_val ≥ 0
    ub_c1 = ub[0] - con_rdm.values
    ub_c2 = ub[1] - con_inc_sales.values

    # Constraints lb: [p_unsub];
    # lb ≤ constr_val <=> constr_val - lb ≥ 0
#     lb_c1 = con_unsub.values - lb[0]

    if scenario == "opt":
        return np.concatenate((ub_c1, ub_c2))
#         return np.concatenate((ub_c1, ub_c2, lb_c1))
    elif scenario == "rpt":
        return np.concatenate((con_rdm.values,
                               con_inc_sales.values,))


def ipop_optimise(best, _base, cols_incl=[]):
    """
    Iterative Profit Optimise (IPOP):

    Constrained optimisation to maximise profit gain.
    For each crn, rank offers in descending order of inc-sales (highest first)
        - For the top two offers, calculate the sales-cost gradient (SCG)
        - Find the optimal sales-cost gradient upper bound (SCGUB)
        - If SCG ≤ SCGUB select the 2nd best offer; otherwise, retain the best
        - If there is an offer switch, reassign the 2nd best offer as the best
        - Identify the next 2nd best offer (e.g. 3rd best)
    Repeat until the number of non-improving evaluations meets a threshold
    (for all crns)

    Args:
        best (df): initial best crn-offer
        _base (df): corresponding crn-offers;
            _prefix to retain columns for output best
        cols_incl (list): additional columns to include

    Args (Config):
        IPOP_N_LOOP (int): num.iterations
        IPOP_CHANGE_LIM (int): limit on # non-improving offers evaluated
        IPOP_X0 (float): SC gradient UB initial value
        IPOP_LB (list): lower bound on perc loss: [p_unsub]
        IPOP_UB (list): upper bound on perc loss: [p_rdm, p_open]

    Returns:
        res (object):
        res.best_ (df): optimised best crn-offer
        res.base_ (df): updated base crn-offers
        res.iter_ (int): final iteration
        res.x_opt_ (float): final iteration x_opt
        res.opt_ (dict): report metrics for final iteration
    """
    pd.options.mode.chained_assignment = None
    
    opt_cols = [
        "crn",  # 'offerId',
        "inc_sales", "target_cost", "score",
        "p_rdm", #"p_open", "p_unsub",
    ] + cols_incl
    
    sort_cols = ["crn", "inc_sales", "target_cost", "score"]
    sort_dir = [True, False, True, False]
    metric_cols = ["inc_sales", "target_cost"]
    constr_cols = ["p_rdm", "inc_sales"]

    best = best[opt_cols]
    base = _base[opt_cols]

    # Initial best offer metrics and constraints
    best0_metrics = best[metric_cols].sum()
    best0_metrics["profit"] = best0_metrics[0] - best0_metrics[1]
    best0_constr_metrics = best[constr_cols].sum()

    # Sort base by "inc_sales"; tie break on "target_cost", "score"
    base.sort_values(by=sort_cols, ascending=sort_dir, inplace=True)

    base["rank"] = (
        base.groupby("crn")[sort_cols[1]]
        .rank(ascending=sort_dir[1], method="first")
        .astype("uint8")
    )
    base["rank_best"] = 1
    base["rank_2nd"] = 2
    base["no_change"] = 0

    offer_cnt = base.groupby("crn")["rank"].max().values
    offer_cnt_explode = [[x] * x for x in offer_cnt]
    base["offer_cnt"] = [x for ls in offer_cnt_explode for x in ls]

    for i in range(Config.IPOP_N_LOOP):

        cond_top2 = (
            (base["rank"] == base["rank_best"])
            | (base["rank"] == base["rank_2nd"])
        )
        top2 = base[cond_top2]

        # Top 2 diff for SC gradient
        top2_diff = top2[metric_cols] - \
            top2.groupby("crn")[metric_cols].shift()
        top2 = top2.merge(
            top2_diff, left_index=True, right_index=True,
            suffixes=("", "_diff")
        )
        top2["sc_grad"] = top2["inc_sales_diff"] / top2["target_cost_diff"]

        x_opt = optimize.fmin_cobyla(
            _obj_func,
            Config.IPOP_X0,
            _constr_ineq,
            args=(top2, best0_metrics),
            consargs=(top2, best0_constr_metrics,
                      Config.IPOP_LB, Config.IPOP_UB),
        )
        best = _choose_offer(top2, x_opt)

        # Check constraints violation where values impossible for all x
        con = _constr_ineq(
            x_opt, top2, best0_constr_metrics, Config.IPOP_LB, Config.IPOP_UB)

        print(f"iter: {i+1}, x_opt: {x_opt.round(4)}")

        # -------------------------------------------------
        # Update base table where best has changed
        # -------------------------------------------------
        cond_offers_exist = base["rank_2nd"] < base["offer_cnt"]

        # Check if num.offer.changes has reached limit
        crn_changes_open = base.groupby(
            "crn")["no_change"].sum() < Config.IPOP_CHANGE_LIM
        cond_changes_open = (
            base["crn"].isin(crn_changes_open[crn_changes_open].index)
            & cond_offers_exist
        )

        # Offer change
        cond = best["rank"] == best["rank_2nd"]
        crn_upd = best.loc[cond, "crn"]
        cond_change = base["crn"].isin(crn_upd)

        # Update best offer where offer changed (2nd becomes best)
        cond = cond_changes_open & cond_change
        base["rank_best"] = np.where(
            cond, base["rank_2nd"], base["rank_best"])

        # Flag no offer change
        cond = cond_changes_open & ~cond_change & (
            base["rank"] == base["rank_2nd"])
        base["no_change"] = np.where(cond, 1, base["no_change"])

        # Identify next 2nd best
        base["rank_2nd"] = np.where(
            cond_changes_open, base["rank_2nd"] + 1, base["rank_2nd"])

        # Terminate: offers exhausted or constraints met
        if (cond_changes_open.sum() == 0) | np.any(con < 0):
            break

    class Res(object):
        pass
    res = Res()
    res.best_ = _base.loc[best.index, :]
    res.base_ = base
    res.iter_ = i
    res.x_opt_ = x_opt
    res.rpt_ = _obj_func(x_opt, top2, best0_metrics, "rpt")
    res.con_ = _constr_ineq(
        x_opt, top2, best0_constr_metrics,
        Config.IPOP_LB, Config.IPOP_UB, scenario="rpt")

    if np.any(con < 0):
        print("IPOP terminated: constraint(s) met")
    elif cond_changes_open.sum() == 0:
        print("IPOP successful")
    elif i + 1 == Config.IPOP_N_LOOP:
        print("IPOP reached iteration limit")
    else:
        print("IPOP Error")

    # profit-gain, cost-saved, inc-sales-loss
    keys = list(res.rpt_.keys())[5:2:-1]
    values = [res.rpt_[k][0].round(2) for k in keys]
    txt = [k.rpartition("_")[0] + ": " + str(v)
           + "%" for k, v in zip(keys, values)]
    print(f'Final state: {txt}')

    # constraints
    txt = [k + "_loss: " + str(v.round(2)) + "%" for k,
           v in zip(constr_cols, res.con_)]
    print(f'Constraints: {txt}')

    return res